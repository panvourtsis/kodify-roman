var test = require('tape');
var romanNumbers = require('../romanNumbers');

test("initial checks", function (t) {
    t.plan(5);

    const valueTest1 = new romanNumbers('I');

    t.equal(valueTest1.toInt(), 1, 'should be equal to 1 because we still return new object');
    t.throws(function() { new romanNumbers(4000); }, 'should be out of range');
    t.throws(function() { new romanNumbers(null); }, 'should require value');
    t.throws(function() { new romanNumbers(); }, 'should require value');
    t.throws(function() { new romanNumbers(0); }, 'should require value');
});

test("roman to integers", function (t) {
    t.plan(11);

    const valueTest1 = new romanNumbers('I');
    const valueTest2 = new romanNumbers('III');
    const valueTest3 = new romanNumbers('IIII');
    const valueTest4 = new romanNumbers('IV');
    const valueTest5 = new romanNumbers('V');
    const valueTest6 = new romanNumbers('CDXXIX');
    const valueTest7 = new romanNumbers('MCDLXXXII');
    const valueTest8 = new romanNumbers('MCMLXXX');
    const valueTest9 = new romanNumbers('MMMMCMXCIX');
    const valueTest10 = new romanNumbers('MMMMDMXCIX');
    const valueTest11 = new romanNumbers('error');

    t.equal(valueTest1.toInt(), 1);
    t.equal(valueTest2.toInt(), 3);
    t.throws(function() { valueTest3.toInt() }, 'should be invalid value');
    t.equal(valueTest4.toInt(), 4);
    t.equal(valueTest5.toInt(), 5);
    t.equal(valueTest6.toInt(), 429);
    t.equal(valueTest7.toInt(), 1482);
    t.equal(valueTest8.toInt(), 1980);
    t.equal(valueTest9.toInt(), 4999);
    t.throws(function() { valueTest10.toInt()  }, 'should throw exception for invalid value');
    t.throws(function() { valueTest11.toInt() }, 'should throw exception for error');
});

test("integers to roman ", function (t) {
    t.plan(9);

    const valueTest1 = new romanNumbers(1968);
    const valueTest2 = new romanNumbers(1473);
    const valueTest3 = new romanNumbers(2999);
    const valueTest4 = new romanNumbers(3000);

    const valueTest5 = new romanNumbers(1);
    const valueTest6 = new romanNumbers(2);
    const valueTest7 = new romanNumbers(3);
    const valueTest8 = new romanNumbers(4);
    const valueTest9 = new romanNumbers(5);

    t.equal(valueTest1.toString(), 'MCMLXVIII');
    t.equal(valueTest2.toString(), 'MCDLXXIII');
    t.equal(valueTest3.toString(), 'MMCMXCIX');
    t.equal(valueTest4.toString(), 'MMM');
    t.equal(valueTest5.toString(), 'I');
    t.equal(valueTest6.toString(), 'II');
    t.equal(valueTest7.toString(), 'III');
    t.equal(valueTest8.toString(), 'IV');
    t.equal(valueTest9.toString(), 'V');
});