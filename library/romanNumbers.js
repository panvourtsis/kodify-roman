/*jslint
    es6, this, for
*/

// i could include lodash for checks and mapping ?
// all checks against http://www.rapidtables.com/convert/number/roman-numerals-converter.htm
// also http://www.onlineconversion.com/roman_numerals_advanced.htm

const romanMapping = [
    {
        "char": "CM",
        "value": 900
    },
    {
        "char": "CD",
        "value": 400
    },
    {
        "char": "XC",
        "value": 90
    },
    {
        "char": "XL",
        "value": 40
    },
    {
        "char": "M",
        "value": 1000
    },
    {
        "char": "D",
        "value": 500
    },
    {
        "char": "C",
        "value": 100
    },
    {
        "char": "L",
        "value": 50
    },
    {
        "char": "X",
        "value": 10
    },
    {
        "char": "IX",
        "value": 9
    },
    {
        "char": "VIII",
        "value": 8
    },
    {
        "char": "VII",
        "value": 7
    },
    {
        "char": "VI",
        "value": 6
    },
    {
        "char": "V",
        "value": 5
    },
    {
        "char": "IV",
        "value": 4
    },
    {
        "char": "III",
        "value": 3
    },
    {
        "char": "II",
        "value": 2
    },
    {
        "char": "I",
        "value": 1
    }
];

function RomanNumber(value) {
    if(!(this instanceof RomanNumber)) {
        return new RomanNumber(value);
    }
    if (!value) {
        throw new Error("value required");
    }
    if (isNumber(value) && (value < 1 || value > 3999)) { // on output
        throw new Error("invalid range");
    }

    this.value = value;

    this.getValue = function () {
        return this.value;
    };

    this.toInt = function () {
        if (!isString(this.getValue()) || !isRoman(this.getValue())) {
            throw new Error("invalid value");
        }
        const tempValue = this.getValue()
            .toUpperCase()
            .split("");

        return tempValue
            .reduce(function (acc, char, index) {
                let tempFind = tempValue[index + 1]
                    ? fetchRomanFromValue(char + tempValue[index + 1]) || fetchRomanFromValue(char)
                    : fetchRomanFromValue(char);

                if (tempValue[index + 1] && fetchRomanFromValue(char + tempValue[index + 1]) ) {
                    tempValue.splice(index + 1, 1);
                }
                return acc + tempFind.value;
            }, 0);
    };

    this.toString = function () {
        if (!isNumber(this.getValue())) {
            throw new Error("invalid value");
        }

        const tempValueArray = this.getValue()
            .toString()
            .split("");

        return tempValueArray
            .reduce(function (acc, char, index) {
                if (parseInt(char) === 0) {
                    return acc;
                }
                const calcZeros = tempValueArray.length - (index + 1);
                let newChar = parseInt(addZeros(calcZeros, char));
                let finalValue = calculateRomanInt(newChar);

                return acc + finalValue;
            }, "");
    };

    function calculateRomanInt(newChar) {
        let finalValue = "";

        const t = romanMapping
            .sort(function(a, b) {
                return b.value - a.value;
            })
            .find(function (val) {
                return val.value <= newChar;
            });

        if (t) {
            newChar = newChar - t.value;

            if (newChar > 0) {
                finalValue += t.char;
                finalValue += calculateRomanInt(newChar);
            } else if (newChar === 0) {
                finalValue += t.char;
            }
        }

        return finalValue;
    }

    function isNumber(value) {
        return Math.round(value) === value;
    }

    function isString(value) {
        return typeof value === "string";
    }

    function isRoman(value) {
        return (
            !(value.indexOf("LC") !== -1 || value.indexOf("DM") !== -1 )
            && !((/I{4}/).test(value) || (/V{2}/).test(value))
        ); // if has more than 3 I is not roman && has two V
    }

    function fetchRomanFromValue(char) {
        return romanMapping.find(function(roman) {
            return roman.char === char;
        });
    }

    function addZeros(count, value) {
        for(let i = 0; i < count; i++) {
            value += "0";
        }
        return value;
    }
}

module.exports = RomanNumber;